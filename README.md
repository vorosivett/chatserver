<table>
<colgroup>
<col width="100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><div id="projectname">
ChatServer
</div></td>
</tr>
</tbody>
</table>

[Public Member Functions](#pub-methods) | [List of all
members](classTChatServiceHandler-members.html)

TChatServiceHandler Class Reference

Inherits TChatService.ISync.

<span id="pub-methods"></span> Public Member Functions
------------------------------------------------------

void 
<a href="classTChatServiceHandler.html#afeae150d4d81f07d503db8aa8436277a" class="el">checkConnection</a>
()
 
string 
<a href="classTChatServiceHandler.html#a4eaec73909d15bf916c8f3180f43d169" class="el">registerClient</a>
(string userName)
 
Register a new client to the server. The Client is identified by its
userName. [More...](#a4eaec73909d15bf916c8f3180f43d169)  
 
List&lt; string &gt; 
<a href="classTChatServiceHandler.html#a4bfba62b62ba9c10ac6a45efa6d3f957" class="el">getClients</a>
(string authID)
 
This method returns the list of the currently registered clients.
[More...](#a4bfba62b62ba9c10ac6a45efa6d3f957)  
 
void 
<a href="classTChatServiceHandler.html#ae45f9bcd73b6cac08631c933943a7027" class="el">sendMessage</a>
(string authID, string receiver, string message)
 
This method sends a message to a registered user.
[More...](#ae45f9bcd73b6cac08631c933943a7027)  
 
List&lt; Message &gt; 
<a href="classTChatServiceHandler.html#a62190f66e39f62f6ddddac99b1d5ba8e" class="el">recvMessage</a>
(string authID)
 
This method returns all the current messages in the user's queue.
[More...](#a62190f66e39f62f6ddddac99b1d5ba8e)  
 
<span id="details"></span>
Detailed Description
--------------------

This is the Main Server Class. This class provides the interface for the
chat clients. The clients can connect to this class and send messages to
each other.

Member Function Documentation
-----------------------------

<span id="afeae150d4d81f07d503db8aa8436277a"></span>
<span class="permalink">[◆ ](#afeae150d4d81f07d503db8aa8436277a)</span>checkConnection()
----------------------------------------------------------------------------------------

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<tbody>
<tr class="odd">
<td>void TChatServiceHandler.checkConnection</td>
<td>(</td>
<td></td>
<td>)</td>
<td></td>
</tr>
</tbody>
</table></td>
<td><span class="mlabels"><span class="mlabel">inline</span></span></td>
</tr>
</tbody>
</table>

This method tests the connection to the server. Call this method after
establishing the connection to make sure that the connection is working.

<span id="a4bfba62b62ba9c10ac6a45efa6d3f957"></span>
<span class="permalink">[◆ ](#a4bfba62b62ba9c10ac6a45efa6d3f957)</span>getClients()
-----------------------------------------------------------------------------------

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<tbody>
<tr class="odd">
<td>List&lt;string&gt; TChatServiceHandler.getClients</td>
<td>(</td>
<td>string </td>
<td><em>authID</em></td>
<td>)</td>
<td></td>
</tr>
</tbody>
</table></td>
<td><span class="mlabels"><span class="mlabel">inline</span></span></td>
</tr>
</tbody>
</table>

This method returns the list of the currently registered clients.

Parameters  
<table>
<tbody>
<tr class="odd">
<td>authID</td>
<td>is the client's authentication id for the current session. This id is generated during the registration.</td>
</tr>
</tbody>
</table>

<!-- -->

Returns  
The list of the registered users including the current user.

<span id="a62190f66e39f62f6ddddac99b1d5ba8e"></span>
<span class="permalink">[◆ ](#a62190f66e39f62f6ddddac99b1d5ba8e)</span>recvMessage()
------------------------------------------------------------------------------------

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<tbody>
<tr class="odd">
<td>List&lt;Message&gt; TChatServiceHandler.recvMessage</td>
<td>(</td>
<td>string </td>
<td><em>authID</em></td>
<td>)</td>
<td></td>
</tr>
</tbody>
</table></td>
<td><span class="mlabels"><span class="mlabel">inline</span></span></td>
</tr>
</tbody>
</table>

This method returns all the current messages in the user's queue.

Parameters  
<table>
<tbody>
<tr class="odd">
<td>authID</td>
<td>The session authentication id of the user.</td>
</tr>
</tbody>
</table>

<!-- -->

Returns  
The list of messages for the user. The server deletes the messages after
the delivery. If there are no messages for the user. This method returns
an empty list.

<span id="a4eaec73909d15bf916c8f3180f43d169"></span>
<span class="permalink">[◆ ](#a4eaec73909d15bf916c8f3180f43d169)</span>registerClient()
---------------------------------------------------------------------------------------

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<tbody>
<tr class="odd">
<td>string TChatServiceHandler.registerClient</td>
<td>(</td>
<td>string </td>
<td><em>userName</em></td>
<td>)</td>
<td></td>
</tr>
</tbody>
</table></td>
<td><span class="mlabels"><span class="mlabel">inline</span></span></td>
</tr>
</tbody>
</table>

Register a new client to the server. The Client is identified by its
userName.

Parameters  
<table>
<tbody>
<tr class="odd">
<td>userName</td>
<td>The username of the new user.</td>
</tr>
</tbody>
</table>

<!-- -->

Returns  
The authentication id generated for the session. You can perform every
action using this unique id.

<!-- -->

Note  
If the client is already registered, then the method returns the current
authentication id.

<span id="ae45f9bcd73b6cac08631c933943a7027"></span>
<span class="permalink">[◆ ](#ae45f9bcd73b6cac08631c933943a7027)</span>sendMessage()
------------------------------------------------------------------------------------

<table>
<colgroup>
<col width="50%" />
<col width="50%" />
</colgroup>
<tbody>
<tr class="odd">
<td><table>
<tbody>
<tr class="odd">
<td>void TChatServiceHandler.sendMessage</td>
<td>(</td>
<td>string </td>
<td><em>authID</em>,</td>
</tr>
<tr class="even">
<td></td>
<td></td>
<td>string </td>
<td><em>receiver</em>,</td>
</tr>
<tr class="odd">
<td></td>
<td></td>
<td>string </td>
<td><em>message</em> </td>
</tr>
<tr class="even">
<td></td>
<td>)</td>
<td></td>
<td></td>
</tr>
</tbody>
</table></td>
<td><span class="mlabels"><span class="mlabel">inline</span></span></td>
</tr>
</tbody>
</table>

This method sends a message to a registered user.

Parameters  
<table>
<tbody>
<tr class="odd">
<td>authID</td>
<td>The session authentication id of the sender</td>
</tr>
<tr class="even">
<td>receiver</td>
<td>The username of the receiver of the message.</td>
</tr>
<tr class="odd">
<td>message</td>
<td>The content of the message.</td>
</tr>
</tbody>
</table>

The messages are stored on the server until they are delivered to the
client. If the client disconnects and reconnects the undelivered
messages will be delivered.

------------------------------------------------------------------------

The documentation for this class was generated from the following file:
-   TChatServiceHandler.cs

------------------------------------------------------------------------

Generated by
 [<img src="doxygen.png" alt="doxygen" class="footer" />](http://www.doxygen.org/index.html)
1.8.14
