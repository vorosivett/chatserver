﻿using System;
using System.Threading;
using Thrift;
using Thrift.Transport;
using Thrift.Protocol;
using Thrift.Collections;
using Thrift.Server;


namespace ChatServer
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var messagequeue = new MessageQueue();
                var usermanager = new UserManager();
                var chatservicehandler = new TChatServiceHandler();
                chatservicehandler.m_messagequeue = messagequeue;
                chatservicehandler.m_usermanager = usermanager;
                var processor = new TChatService.Processor(chatservicehandler);
                TServerTransport serverTransport = new TServerSocket(9090);

                // Single for Testing 
                // var server = new TSimpleServer(processor, serverTransport);
                
                // Use this for a multithreaded server
                var server = new TThreadPoolServer(processor, serverTransport);

                Console.WriteLine("Starting the server...");
                server.Serve();
            }
            catch (Exception x)
            {
                Console.WriteLine(x.StackTrace);
                Console.WriteLine(x.What);
            }
            Console.WriteLine("done.");
        }
    }
}
