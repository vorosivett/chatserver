using System;
using System.Text;
using System.Threading;
using System.Collections.Generic;
class UserManager {

    private object m_lock = new object();
    private Random m_random = new System.Random();
    private Dictionary<string, bool> m_userlistupdated = new Dictionary<string, bool>();
    private Dictionary<string, string> m_username2authid = new Dictionary<string, string>();
    private Dictionary<string, string> m_authid2username = new Dictionary<string, string>();
    public string registerUser( string userName ) {
        lock(m_lock) {
            if (m_username2authid.ContainsKey(userName)) {
                m_userlistupdated[userName] = true;
                return m_username2authid[userName];
            } else {
                var authid = genUserAuth();
                m_username2authid.Add(userName, authid);
                m_authid2username.Add(authid, userName);
                m_userlistupdated.Add(userName, false);
                // invalidate all the userlists
                var keys = new List<string>(m_userlistupdated.Keys);
                foreach ( var key in keys ) {
                    m_userlistupdated[key] = true;
                }
                return authid;
            }
        }
    }
    private string genUserAuth() {
        var byteArray = new byte[16];

        // int value = random.Next(0, 100); //returns integer of 0-100
        // double value2 = random.NextDouble(); //returns floating point 0.0-1.0
        m_random.NextBytes(byteArray); //fill with random bytes

        StringBuilder hex = new StringBuilder(byteArray.Length * 2);
        foreach(byte b in byteArray) {
            hex.AppendFormat("{0:x2}", b);
        }

        return hex.ToString();
    }

    public string getUserName (string authid) {
        lock(m_lock) {
            // security check
            if (m_authid2username.ContainsKey(authid)) {
                return m_authid2username[authid];
            } else {
                throw new InvalidAuthenticationToken();
            }
        }
    }

    public List<string> getUserList(string userName) {
        lock( m_lock ) {
            if (m_userlistupdated[userName]) {
                m_userlistupdated[userName] = false;
                return new List<string>(m_username2authid.Keys);
            } else {
                return new List<string>();
            }
        }
    }
}