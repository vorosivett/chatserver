using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Threading;
class MessageQueue {

    private object m_lock = new object();

    private Dictionary<string, Queue<Message> > m_messagequeue = new Dictionary<string, Queue<Message> >();
    public void addMessage( string sender, string receiver, string message ) {
        Debug.Assert(sender!=null);
        Debug.Assert(receiver!=null);
        Debug.Assert(message!=null);
        Message tmessage = new Message();
        tmessage.Sender = sender;
        tmessage.Receiver = receiver;
        tmessage.Msg = message;
        lock(m_lock) {
            if (m_messagequeue.ContainsKey(receiver)) {
                m_messagequeue[receiver].Enqueue(tmessage);
            } else {
                var queue = new Queue<Message>();
                queue.Enqueue(tmessage);
                m_messagequeue.Add(receiver, queue);
            }
            
        }
    }

    public List<Message> getMessage(string receiver) {
        Debug.Assert(receiver!=null);
        lock(m_lock){
            if (! isMessageFor(receiver)) {
                return new List<Message>();
            } else {
                var result = new List<Message>();
                while (m_messagequeue[receiver].Count>0) {
                    result.Add(m_messagequeue[receiver].Dequeue());
                }
                return result;
            }
        }
    }

    bool isMessageFor(string receiver) {
        if (! m_messagequeue.ContainsKey(receiver)) {
            return false;
        }

        if (m_messagequeue[receiver].Count==0) {
            return false;
        }
        return true;
    }
}