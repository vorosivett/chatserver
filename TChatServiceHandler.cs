
using System;
using System.Diagnostics;
using System.Collections.Generic;

class TChatServiceHandler : TChatService.ISync {
    /// <summary>
    /// Missing: random number generator for access
    /// Queue for incoming messages
    /// map for the registered users
    /// </summary>

    public UserManager m_usermanager { get;set;}
    public MessageQueue m_messagequeue { get;set;}
    public void checkConnection() {
        Console.WriteLine("public override void checkConnection");
    }
    public string registerClient(string userName) {
        Console.WriteLine("public override string registerClient(string " + userName + ")");
        return m_usermanager.registerUser(userName);
    }
    public List<string> getClients(string authID) {
        Console.WriteLine("public override List<string> getClients(string authID)");
        string userName = m_usermanager.getUserName(authID);
        Debug.Assert(userName!=null);
        var list = m_usermanager.getUserList(userName);
        Debug.Assert(list!=null);
        return list;
    }
    public void sendMessage(string authID, string receiver, string message) {
        string userName = m_usermanager.getUserName(authID);
        m_messagequeue.addMessage(userName, receiver, message);
        Console.WriteLine("sendMessage { authID: " + authID + ", receiver: " + receiver + ", message: " + message + " }");
    }
    public List<Message> recvMessage(string authID) {
        Console.WriteLine("Message recvMessage(string " + authID + ") ");
        var userName = m_usermanager.getUserName(authID);
        var msg = m_messagequeue.getMessage(userName);
        return msg; 
    }
}
